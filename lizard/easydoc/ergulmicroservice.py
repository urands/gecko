

from pdf417service.parse import ParseErgul
from pdf417service.uploadergul import upload_ergul

class Ergul():

    def get(self, inn):
        pdf = f'ergul/{inn}.pdf'
        if not upload_ergul(pdf):
            return None

        data = ParseErgul().parse(pdf)
        return data
