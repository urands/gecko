from django.http import HttpResponse
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

class HomePageView(LoginRequiredMixin, TemplateView):

    template_name = 'main.html'

    def get(self, request, *args, **kwargs):
        return super(HomePageView,self).get(request,*args,**kwargs)