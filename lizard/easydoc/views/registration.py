from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Required')

    class Meta:
        model = User
        fields = ('email', 'username')


from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.conf import settings


from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


from easydoc.ergulmicroservice import Ergul

class RegisterView(TemplateView):

    template_name = 'registration/register.html'

    def get(self, request, *args, **kwargs):
        return super(RegisterView,self).get(request, *args,**kwargs)

    def post(self, request, *args, **kwargs):
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            password = form.cleaned_data.get('password')
            user.set_password(password)
            user.save()
            ergul = Ergul().get(user.username)
            if ergul is None:
                return redirect('createinn')
            else:
                return redirect('home')

        return redirect('register')


class LoginView(TemplateView):

    template_name = 'registration/login.html'

    def get(self, request, *args, **kwargs):
        return super(LoginView,self).get(request, *args,**kwargs)

    def post(self, request, *args, **kwargs):
        if 'username' in request.POST:
            username = request.POST['username']
        if 'password' in request.POST:
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')

        return redirect('login')
