from django.http import HttpResponse
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from pdf417service.pdf417 import PrintPDF417

class CreateINNView(LoginRequiredMixin, TemplateView):

    template_name = 'main.html'

    def get(self, request, *args, **kwargs):
        return super(CreateINNView,self).get(request,*args,**kwargs)


    def post(self, request, *args, **kwargs):
        inn = request.POST['inn']
        #xml = render('1112512_5.01000_03.html')
        xml = ''''<?xml version="1.0" encoding="utf8" ?>'; @endphp
        <Файл ИдФайл="1112512" ВерсПрог="Konsensus 1.0" ВерсФорм="5.01">
            <Документ КНД="{{$kdn}}" КодСФРД="Р26001" ДатаДок="{{$datedoc}}">
                <СвИП ОГРНИП="{{$ogrn}}" ИННФЛ="{{$inn}}">
                    <ФИОРус Фамилия="{{$surname}}" Имя="{{$name}}" Отчество="{{$patronymic}}" />
                </СвИП>
                <СпВыдДок>{{$extradition}}</СпВыдДок>
                <Контакт НомТел="{{$phone}}" E-mail="{{$email}}" />
            </Документ>
        </Файл>
        '''
        pdf = PrintPDF417()
        pdf.print(xml,inn,'1112512',)
        return redirect('home')
