from pdfminer.converter import TextConverter, XMLConverter, HTMLConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
from pdfminer.layout import LAParams
import io


class ParseErgul:

    def extract_text_from_pdf(pdf_path):
        resource_manager = PDFResourceManager()
        fake_file_handle = io.StringIO()
        # converter = TextConverter()
        codec = 'utf-8'
        laparams = LAParams(
            char_margin=0.01,  # default 1.0
            word_margin=0.2,  # default 0.2
            line_margin=0.3,  # default 0.3
            line_overlap=0.5  # default 0.5
        )
        converter = TextConverter(resource_manager, fake_file_handle,
                                  codec=codec)  # TextConverter(resource_manager, fake_file_handle)
        page_interpreter = PDFPageInterpreter(resource_manager, converter)

        with open(pdf_path, 'rb') as fh:
            for page in PDFPage.get_pages(fh,
                                          caching=True,
                                          check_extractable=True):
                page_interpreter.process_page(page)

            text = fake_file_handle.getvalue()

        # close open handles
        converter.close()
        fake_file_handle.close()

        if text:
            return text

    def clip(s, sub, clip):
        start = s.find(sub)
        if start >= 0:
            start += len(sub)
            return s[start:clip + start]

    def parse(self, pdfname):

        txt = extract_text_from_pdf('temp.pdf')

        txt = bytearray(txt, 'utf8').decode()

        data = {'inn': '554564645855'}

        data['action'] = self.clip(txt, '21Код и наименование вида деятельности', 2)
        data['ogrnip'] = self.clip(txt, '44ОГРНИП ', 15)
        data['datadoc'] = self.clip(txt, 'Выписка из ЕГРИП', 8)

        name = self.clip(txt, 'Настоящая выписка содержит сведения об индивидуальном предпринимателе', 100)
        addr = self.clip(txt, '11Адрес регистрирующего органа', 100)
        data['address'] = addr[:addr.find('12ГРН')]
        data['fam'], data['name'], data['otc'] = name[:name.find('фамилия')].split()

        return data
