from pdfminer.converter import TextConverter, XMLConverter, HTMLConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
from pdfminer.layout import LAParams
import io


def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    # converter = TextConverter()
    codec = 'utf-8'
    laparams = LAParams(
        char_margin=0.01,  # default 1.0
        word_margin=0.2,  # default 0.2
        line_margin=0.3,  # default 0.3
        line_overlap=0.5  # default 0.5
    )
    converter = TextConverter(resource_manager, fake_file_handle,
                              codec=codec)  # TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)

    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh,
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)

        text = fake_file_handle.getvalue()

    # close open handles
    converter.close()
    fake_file_handle.close()

    if text:
        return text

def clip(s, sub, clip):
    start = s.find(sub)
    if start >=0 :
        start+=len(sub)
        return s[start:clip+start]


txt = extract_text_from_pdf('temp.pdf')

txt = bytearray(txt, 'utf8').decode()


data = {'inn' : '554564645855'}

data['action'] = clip(txt,'21Код и наименование вида деятельности', 2)
data['ogrnip'] = clip(txt,'44ОГРНИП ',15)
data['datadoc'] = clip(txt, 'Выписка из ЕГРИП', 8)

name = clip(txt,'Настоящая выписка содержит сведения об индивидуальном предпринимателе',100)
addr = clip(txt,'11Адрес регистрирующего органа',100)
data['address'] = addr[:addr.find('12ГРН')]
data['fam'], data['name'], data['otc'] = name[:name.find('фамилия')].split()

f = open('to.txt','w')

xml = f'<?xml version="1.0" encoding="utf8" ?><Файл ИдФайл="1150001_5.02000_02" ВерсПрог="Konsensus 1.0" ВерсФорм="5.02"> \
  <Документ КНД="1150001" ДатаДок="13-07-2018" КодНО="0987"> \
    <СвНП Тлф="+7(965)454-54-54" ПрНП="1"> \
      <НПФЛ ИННФЛ="098765432112"> \
		<ФИО Фамилия="' + data['fam']  + '" Имя="'+data['name'] +'" Отчество="'+ data['otc'] +'"></ФИО> \
	  </НПФЛ> \
    </СвНП> \
    <Подписант ПрПодп="1"> \
      <ФИО Фамилия="" Имя="" Отчество=""></ФИО> \
      <СвПред НаимДок=""></СвПред> \
    </Подписант> \
    <ПРХУСН ОбНал="1" ПрДатаПер="2" ГодПерех="" ДатаПерех="" ГодПодач="2018" Доход9М="" ОстСтОснСр=""></ПРХУСН> \
  </Документ></Файл> '

f.write(txt)
f.close()

print(data)

xml = xml.encode('utf8').decode('utf8')

xmlname = data['inn'] + '-1150001-02082018'

f = open(xmlname + '.xml','w')

f.write(xml)

f.close()


import os

cmd = 'convertor.bat 098765432112-1150001-02082018'

#cmd = f'converter.bat {xmlname}'

#print(cmd)
os.system(cmd)