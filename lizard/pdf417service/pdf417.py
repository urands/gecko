import os
from datetime import datetime

class PrintPDF417():
    '''
    Class for printing FORM from xml and tiff to PDF
    '''

    path_output = './xmlout'
    path_pdf = './pdfout'
    path_script = '.'

    def print(self, xml, inn, kdn):
        xml = xml.encode('utf8').decode('utf8')
        filename =f'{inn}-{kdn}-{"%d%m%Y".format(datetime.now())}'
        f = open( os.path.join(self.path_output, filename + '.xml'), 'w')
        f.write(xml)
        f.close()
        cmd = f'{self.path_script}/convertor.bat {filename}'
        os.system(cmd)

        f = open( os.path.join(self.path_output, filename + '.pdf'), 'rb')
        pdf = f.read(1024*1024*20)
        f.close()
        return pdf



