@echo off 

: WINDOWS 95/98/ME
set folder=%WinDir%\system

: WINDOWS NT/2000/XP/2003/2008/7 x64
if not "%SystemRoot%" == "" set folder=%SystemRoot%\SysWOW64

: WINDOWS NT/2000/XP/2003/2008/7 x86
if not "%SystemRoot%" == "" if not exist "%folder%" set folder=%SystemRoot%\system32

: WINDOWS NT/2000/XP/2003/2008/7 x86
set program=%ProgramFiles%\gnivc\print-nd-pdf417

: WINDOWS NT/2000/XP/2003/2008/7 x64CO
if not "%ProgramFiles(x86)%" == "" set program=%ProgramFiles(x86)%\gnivc\print-nd-pdf417

set file=%program%\TAXDOCPrt.dll

"%folder%\cscript.exe" //I //Nologo //Job:testPrint "%~dp0convertor.wsf" %1

:end